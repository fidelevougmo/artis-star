/**
 * @file common/context/State.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_CONTEXT_STATE_HPP
#define COMMON_CONTEXT_STATE_HPP

#include <artis-star/common/context/StateValues.hpp>

#include <map>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

namespace artis {
namespace common {
namespace context {

template<class Time>
class State
{
  typedef std::map<unsigned int, State<Time> > Substates;

public:
  State()
      : _last_time(-1), _next_time(-1)
  {}

  virtual ~State()
  {}

  void add_state(unsigned int variable_key, const Value &value)
  {
    _values.add_state(variable_key, value);
  }

  void add_substate(unsigned int model_key, State &state)
  {
    _substates.insert(std::make_pair(model_key, state));
  }

  const Value &get_state(unsigned int variable_key) const
  {
    return _values.get_state(variable_key);
  }

  const State<Time> &get_substate(unsigned int model_key) const
  {
    return _substates.find(model_key)->second;
  }

  typename Time::type last_time() const
  { return _last_time; }

  void last_time(typename Time::type t)
  { _last_time = t; }

  typename Time::type next_time() const
  { return _next_time; }

  void next_time(typename Time::type t)
  { _next_time = t; }

  std::string to_string() const
  {
    std::string str = "last_time: " + std::to_string(_last_time) +
        "; next_time: " + std::to_string(_next_time) +
        "; values: " + _values.to_string() + "; sub_states: [ ";

    for (typename Substates::const_iterator it = _substates.begin();
         it != _substates.end(); ++it) {
      str += it->second.to_string() + " ";
    }
    str += "]";
    return str;
  }

private:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive &ar, const unsigned int version)
  {
    (void) version;

    ar & _values;
    ar & _substates;
    ar & _last_time;
    ar & _next_time;
  }

  StateValues _values;
  Substates _substates;
  typename Time::type _last_time;
  typename Time::type _next_time;
};

}
}
}

#endif
