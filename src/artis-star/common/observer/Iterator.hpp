/**
 * @file common/observer/View.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_COMMON_OBSERVER_ITERATOR_HPP
#define ARTIS_COMMON_OBSERVER_ITERATOR_HPP

#include <artis-star/common/observer/View.hpp>

namespace artis {
namespace observer {

template<typename Time>
class Iterator
{
public:
  Iterator(const typename View<Time>::Values &view)
      : _view(view), _iterator(view.begin())
  {}

  virtual ~Iterator() = default;

  bool has_next() const
  { return _iterator != _view.end(); }

  virtual void operator++()
  { _iterator++; }

  virtual const std::pair<double, common::Value> &operator*() const
  { return *_iterator; }

private:
  const typename View<Time>::Values &_view;
  typename View<Time>::Values::const_iterator _iterator;
};

template<typename Time>
class DiscreteTimeIterator : public Iterator<Time>
{
public:
  DiscreteTimeIterator(const typename View<Time>::Values &view,
                       const typename Time::type &start, const typename Time::type &step)
      : Iterator<Time>(view), _step(step), _time(start)
  {
    _last_value = &Iterator<Time>::operator*();
    while (Iterator<Time>::operator*().first <= start
        and Iterator<Time>::has_next()) {
      Iterator<Time>::operator++();
      if (Iterator<Time>::operator*().first == start) {
        _last_value = &Iterator<Time>::operator*();
      }
    }
  }

  virtual ~DiscreteTimeIterator() = default;

  void operator++() override
  {
    _time += _step;
    _last_value = &Iterator<Time>::operator*();
    while (Iterator<Time>::operator*().first <= _time and Iterator<Time>::has_next()) {
      Iterator<Time>::operator++();
      if (Iterator<Time>::operator*().first == _time) {
        _last_value = &Iterator<Time>::operator*();
      }
    }
  }

  const std::pair<double, common::Value> &operator*() const override
  { return *_last_value; }

private:
  typename Time::type _step;
  typename Time::type _time;
  const std::pair<double, common::Value> *_last_value;
};

}
}

#endif